// api
const apiPath = 'https://reqres.in/api';

// types
export const FETCH_USER_BEGIN   = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

// actions
// Get user
export function fetchUser(id) {
  return dispatch => {
    dispatch(fetchUserBegin());
    return fetch(apiPath + '/users/' + id)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchUserSuccess(json.data));
        return json.data;
      })
      .catch(error => dispatch(fetchUserFailure(error)));
  };
}

// data returned
export const fetchUserBegin = () => ({
  type: FETCH_USER_BEGIN,
});

export const fetchUserSuccess = user => ({
  type: FETCH_USER_SUCCESS,
  payload: { user },
});

export const fetchUserFailure = error => ({
  type: FETCH_USER_FAILURE,
  payload: { error },
});

// reducers
const initialState = {
  data: [],
  loading: false,
  error: null,
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_USER_BEGIN:
      return { ...state, loading: true, error: null };
    case FETCH_USER_SUCCESS:
      return { ...state, loading: false, data: payload.user };
    case FETCH_USER_FAILURE:
      return { ...state, loading: false, error: payload.error, data: [] };
    default:
      return state;
  }
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
