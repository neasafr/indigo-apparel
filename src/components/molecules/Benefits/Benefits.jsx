import React from 'react';
// Styles
import './Benefits.css';

const Benefits = () => (
  <div className="benefits h2">
    <div className="benefit">
      <i className="fa fa-box-open" />
      <p>Free UK delivery + returns</p>
    </div>
    <div className="benefit">
      <i className="fa fa-graduation-cap" />
      <p>15% Student discounts</p>
    </div>
    <div className="benefit">
      <i className="fa fa-truck" />
      <p>Fast and reliable deliveries</p>
    </div>
 </div>
);

export default Benefits;
