import React from 'react';
import PropTypes from 'prop-types';
// Styles
import './Product.css';
// Image
const triangleShape = (
  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="365.59375px" height="387px" viewBox="0.00024 0 365.59375 387" enableBackground="new 0.00024 0 365.59375 387"
     xmlSpace="preserve" className="triangle">
    <g>
      <line fill="none" stroke="#FFFFFF" strokeWidth="27" strokeMiterlimit="10" x1="-35.70288" y1="46" x2="372.50513" y2="412"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="24" strokeMiterlimit="10" x1="-59.52222" y1="69.81934" x2="348.68579" y2="435.81934"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="21" strokeMiterlimit="10" x1="-83.34204" y1="93.63916" x2="324.86646" y2="459.63867"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="18" strokeMiterlimit="10" x1="-107.16138" y1="117.4585" x2="301.04712" y2="483.45801"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="15" strokeMiterlimit="10" x1="-130.98071" y1="141.27783" x2="277.22681" y2="507.27832"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="12" strokeMiterlimit="10" x1="-154.80005" y1="165.09717" x2="253.40747" y2="531.09766"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="9" strokeMiterlimit="10" x1="-178.61987" y1="188.91699" x2="229.58813" y2="554.91699"/>
      
        <line fill="none" stroke="#FFFFFF" strokeWidth="6" strokeMiterlimit="10" x1="-202.43921" y1="212.73633" x2="205.7688" y2="578.73633"/>
    </g>
  </svg>
);

const Product = ({ data: { name, price, image } }) => (
  <div className="product">
    <div className="image-container">
      <img src={image} alt={name} />
      {triangleShape}
    </div>
    <h4>{name}</h4>
    <p>£{price}</p>
  </div>
);

Product.propTypes = {
  data: PropTypes.object,
};

Product.defaultProps = {
  data: {},
};

export default Product;
