import React, { Component } from 'react';
// images
import triangleShapeWhite from '../../assets/triangle-shape-white.svg';
import triangleShape from '../../assets/triangle-shape.svg';
import triangleShapeLeft from '../../assets/triangle-shape-left.svg';
import clothesBanner from '../../assets/clothes/banner.jpg';
// Components
import TitleSubtitle from '../../components/atoms/TitleSubtitle/TitleSubtitle';
import Product from '../../components/atoms/Product/Product';
import Filter from '../../components/atoms/Filter/Filter';
import Benefits from '../../components/molecules/Benefits/Benefits';
import SocialPanel from '../../components/molecules/SocialPanel/SocialPanel';
// Data
import { clothes } from '../../data/clothes';
// Styles
import './Clothes.css';

class Clothes extends Component {
  constructor(props) {
    super(props);

    this.state = { pace1: '', pace2: '' };

    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    document.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
      const pace1 = `translateY(-${document.documentElement.scrollTop / 6.5}px)`;
      const pace2 = `translateY(-${document.documentElement.scrollTop / 2.5}px)`;

      this.setState({ pace1, pace2 });
  }

  render() {
    const { pace1, pace2 } = this.state;

    return (
      <div className="clothes">
        <div className="col-14 banner" style={{backgroundImage: `url(${clothesBanner})`}}>
          <TitleSubtitle title="Feeling like yourself" subtitle="Clothes" />
          <img src={triangleShapeWhite} alt="brand shape" className="triangle-shape" />
        </div>

        <div className="main">
          <Filter />
          <div className="product-list col-14 offset-1">
            {clothes.map(cloth => <Product key={cloth.id} data={cloth} />)}
          </div>
          <SocialPanel />
          <Benefits />
        </div>
        <img src={triangleShapeLeft} alt="brand shape" className="triangle-parallax" style={{ transform: pace1 }} />
        <img src={triangleShape} alt="brand shape" className="triangle-parallax right" style={{ transform: pace2 }} />
      </div>
    );
  }
}

export default Clothes;
