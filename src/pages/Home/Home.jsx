import React, { Component } from 'react';
// images
import triangleShape from '../../assets/triangle-shape.svg';
import promo01 from '../../assets/promo/promo01.jpg';
import promo02 from '../../assets/promo/promo02.jpg';
import promo03 from '../../assets/promo/promo03.jpg';
import slideImage01 from '../../assets/slider/slide01.png';
import slideImage02 from '../../assets/slider/slide02.png';
import slideImage03 from '../../assets/slider/slide03.jpg';
import slidePromoImage01 from '../../assets/slider/promo01.jpg';
import slidePromoImage02 from '../../assets/slider/promo02.jpg';
// Components
import { CarouselProvider, Slider, Slide } from 'pure-react-carousel';
import TitleSubtitle from '../../components/atoms/TitleSubtitle/TitleSubtitle';
import PromoBlock from '../../components/molecules/PromoBlock/PromoBlock';
import Benefits from '../../components/molecules/Benefits/Benefits';
import SocialPanel from '../../components/molecules/SocialPanel/SocialPanel';
// Styles
import 'pure-react-carousel/dist/react-carousel.es.css';
import './Home.css';
import '../../style/Slide.scss';

class Home extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className="homepage">
        <CarouselProvider
          totalSlides={3}
          isPlaying
          naturalSlideHeight={245}
          naturalSlideWidth={100}
         className="offset-2 offset-2-xs col-14 col-14-xs"
        >
          <Slider>
            <Slide index={0}>
              <div className="slide-inner align-top-w768" style={{backgroundImage: `url(${slideImage01})` }}>        
                <TitleSubtitle title="Feel string in Safari" subtitle="Latest trends" />
             </div>
            </Slide>
            <Slide index={1}>
              <div className="slide-inner align-top-w768" style={{backgroundImage: `url(${slideImage02})` }}>        
                <TitleSubtitle title="Be stylise this Summer" subtitle="Accessories" />
             </div>
            </Slide>
            <Slide index={2}>
              <div className="slide-inner align-top-w768" style={{backgroundImage: `url(${slideImage03})` }}>        
                <TitleSubtitle title="Spring denim trends" subtitle="Denim" />
             </div>
            </Slide>
          </Slider>
        </CarouselProvider>

        <div className="main">
          <CarouselProvider
            totalSlides={2}
            isPlaying
            naturalSlideHeight={200}
            naturalSlideWidth={100}
            className="col-6 col-4-w1024 promo-carousel"
          >
            <Slider>
              <Slide index={0}>
                <div className="slide-inner" style={{backgroundImage: `url(${slidePromoImage01})` }}>        
                  <TitleSubtitle title="Be Festival ready!" subtitle="Latest trends" />
               </div>
               </Slide>
              <Slide imdex={1}>
                <div className="slide-inner align-top-w768" style={{backgroundImage: `url(${slidePromoImage02})` }}>        
                  <TitleSubtitle title="Dive in to Summer" subtitle="Swimwear" />
               </div>
               </Slide>
            </Slider>
          </CarouselProvider>
          <div className="promo-container offset-2-xs offset-0-w768 offset-2-w1024 col-14-xs col-8-w768">
            <h3>Get summer ready</h3>
            <PromoBlock image={promo01} title="Beauty" subtitle="Feel beautiful" />
            <PromoBlock image={promo02} title="Shoes" subtitle="Walk tall" />
            <PromoBlock image={promo03} title="Lifestyle" subtitle="Live life" />
            <div className="benefits">
            </div>
            <Benefits />
          </div>
          <img src={triangleShape} alt="brand shape" className="triangle-shape" />
          <SocialPanel />
        </div>
     </div>
    );
  }
}

export default Home;
