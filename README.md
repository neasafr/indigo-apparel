This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Start-up project

You need to first install the packages

### `npm install`

and then run

### `npm start`

this will open your browser for the dev environment.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run watch-css`
### `npm run build-css`

This must be running simultaneously with the `npm start` to complile the styles, or the app will not appear as intended<br>
Or used to initialize the build of SCSS files.
